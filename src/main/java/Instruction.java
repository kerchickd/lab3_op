public class Instruction {
    public String instruction;
    public long address;

    public Instruction(String instruction, long address) {
        this.instruction = instruction;
        this.address = address;
    }
}
