/* It is in this file, specifically the replacePage function that will
   be called by MemoryManagement when there is a page fault.  The 
   users of this program should rewrite PageFault to implement the 
   page replacement algorithm.
*/

// This PageFault file is an example of the LRU (based on counter field) Page Replacement
// Algorithm as described in the Memory Management section.

import java.util.Vector;

public class PageFault {

    /**
     * LRU Based on counter field (Kirill Kotsko impl)
     *
     * @param mem            is the vector which contains the contents of the pages
     *                       in memory being simulated.  mem should be searched to find the
     *                       proper page to remove, and modified to reflect any changes.
     * @param replacePageNum is the requested page which caused the
     *                       page fault.
     * @param controlPanel   represents the graphical element of the
     *                       simulator, and allows one to modify the current display.
     */
    public static void replacePage(Vector<Page> mem, int replacePageNum, ControlPanel controlPanel) {
        Page pageToSet;
        Page nextProbablePage;
        int amountOfPhysicalPage = 0;

        for (var x : mem)
            if (x.physical == -1)
                amountOfPhysicalPage++;

        if (amountOfPhysicalPage < (mem.size() / 2 + 1)) {
            int oldestUsedPage = Integer.MIN_VALUE;
            int oldestUsedTime = 0;
            int firstPage = Integer.MIN_VALUE;
            for (var x : mem) {
                if (x.physical != -1) {
                    if (firstPage == Integer.MIN_VALUE)
                        firstPage = mem.indexOf(x);

                    if (x.lastTouchTime > oldestUsedTime) {
                        oldestUsedTime = x.lastTouchTime;
                        oldestUsedPage = mem.indexOf(x);
                    }
                }
            }
            if (oldestUsedPage == Integer.MIN_VALUE)
                oldestUsedPage = firstPage;
            pageToSet = mem.elementAt(oldestUsedPage);
            nextProbablePage = mem.elementAt(replacePageNum);
            controlPanel.removePhysicalPage(oldestUsedPage);
            nextProbablePage.physical = pageToSet.physical;
            controlPanel.addPhysicalPage(nextProbablePage.physical, replacePageNum);

            pageToSet.physical = -1;
            pageToSet.lastTouchTime = 0;

            pageToSet.R = 0;
            pageToSet.M = 0;
            pageToSet.inMemTime = 0;
        } else {
            pageToSet = mem.elementAt(replacePageNum);
            pageToSet.physical = mem.size() - amountOfPhysicalPage;
            controlPanel.addPhysicalPage(pageToSet.physical, replacePageNum);
        }
    }
}
